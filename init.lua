local api = nc_sky_ultra_hard
api.barrier_ymin = -math.huge
api.barrier_ymax = -math.huge
api.assign_ttl = -1

-- increasingly cursed hacks follow

local cursing = false

local store_mt = getmetatable(minetest.get_mod_storage())
local store_set_string = store_mt.set_string
local hellstore
local helldata = {}

local lastdata
local serialize = minetest.serialize
function minetest.serialize(t,...)
	if cursing then
		lastdata = t
	end
	return serialize(t,...)
end

function store_mt:set_string(k, v, ...)
	if cursing then
		hellstore = self
		helldata[k] = lastdata
	end
	if self == hellstore and k ~= "islands_by_player" then
		v = minetest.serialize({})
	end
	return store_set_string(self, k, v, ...)
end

local send_to_island = api.send_to_island
local give_island = api.give_island

local function wipe(t)
	for k,v in pairs(t) do t[k]=nil end
	return t
end

function api.give_island(player)
	assert(not cursing)
	cursing = true
	local send_to_island = api.send_to_island
	api.send_to_island = function() end
	give_island(player)
	setmetatable(wipe(helldata.islands_by_pos),{
		__index = function() return nil end,
		__newindex = function() end
	})
	api.send_to_island = send_to_island
	api.send_to_island(player)
	api.give_island = give_island
	cursing = false
end

local island_near = api.island_near
function api.island_near(pos,...)
	local nullis = api.island_grid_round(vector.new(0,(api.islands_ymin+api.islands_ymax)/2,0),...)
	local out = api.island_grid_round(pos,...)
	if not vector.equals(api.island_grid_round(pos,...),nullis) then
		return
	end
	return island_near(pos,...)
end
